<?php declare(strict_types=1);

namespace Tests\Connecting;

use Paycoiner\Client\Clients\PayoutClient;
use Paycoiner\Client\Models\Requests\PayoutRequest;
use Tests\TestCase;

class PayoutTestConnecting extends TestCase
{
    public function testCreate()
    {
        $customerId = '00e55aac-2e7d-11e8-a650-507b9dfbcb8f';
        $orderId = time() . md5((string) time());
        $client = new PayoutClient('http://payouts-api.paycoiner.loc', __DIR__ . '/../mocks/keys/private_key.pem');
        $response = $client->create(new PayoutRequest(
            $customerId,
            $orderId,
            md5((string) time()) . md5((string) rand(0, 1000)),
            'BTC',
            '0.00001'
        ));

        $this->assertSame($customerId, $response->getCustomerId());
        $this->assertSame($orderId, $response->getOrderId());
        $this->assertNotEmpty($response->getPayoutId());
    }
}
