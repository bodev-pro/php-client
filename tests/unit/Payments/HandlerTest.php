<?php declare(strict_types=1);

namespace Tests\Unit\Payments;

use Paycoiner\Client\Enums\InvoiceStatus;
use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Handlers\PaymentHandler;
use Paycoiner\Client\Models\Responses\Invoice;
use Paycoiner\Client\Services\HmacService;
use Tests\TestCase;

class HandlerTest extends TestCase
{
    protected function tearDown()
    {
        unset($_SERVER['HTTP_HMAC']);
        parent::tearDown();
    }

    private function getRawData(): string
    {
        return file_get_contents(__DIR__ . '/../../mocks/payments/invoice.json');
    }

    private function getData(): array
    {
        return json_decode($this->getRawData(), true);
    }

    public function testHmacNotSet()
    {
        $this->expectException(InvalidHash::class);
        (new PaymentHandler(''))->handle($this->getData());
    }

    public function testInvalidHmac()
    {
        $_SERVER['HTTP_HMAC'] = 'dummy';
        $this->expectException(InvalidHash::class);
        (new PaymentHandler(self::WEBHOOK_KEY))->handle($this->getData());
    }

    public function testInvalidWebhook()
    {
        $data = $this->getData();
        $_SERVER['HTTP_HMAC'] = (new HmacService())->getHmac($this->getRawData(), self::WEBHOOK_KEY);
        $this->expectException(InvalidHash::class);
        (new PaymentHandler('dummy'))->handle($data);
    }

    public function testValidWebhook()
    {
        $data = $this->getData();
        $_SERVER['HTTP_HMAC'] = (new HmacService())->getHmac($this->getRawData(), self::WEBHOOK_KEY);
        $this->expectException(InvalidHash::class);
        (new PaymentHandler(self::WEBHOOK_KEY))->handle($data);
    }

    protected function assertInvoiceResponse(Invoice $response)
    {
        $this->assertSame('1547473364088', $response->getOrderId());
        $this->assertSame('BTC', $response->getCurrency());
        $this->assertSame('2N9xESQREzgsbSyY78C2RoshV1qA4L3kY2o', $response->getAddress());
        $this->assertSame(
            'o34CoumNxCmnYMVd8qu2LQfWLdY3IwXrnUzYa01yu1l+tnDrnpH1S/svFy9p/abkMksLpgyC9KvGdAIvw/gFQ3K1Z8/NRbKZWqJN2Tt4AuZfeDI0n3B/VCNkmLDIPhu0lPnlJEsb9KAvPNlCPcYuLdO0VKUag6cVz41lv7AQAaM=',
            $response->getSignature()
        );
        $this->assertSame('45511162-1802-11e9-ae97-fa163e025a6a', $response->getInvoiceId());
        $this->assertSame('https://demo.paycoiner.com/invoice/view/?id=45511162-1802-11e9-ae97-fa163e025a6a', $response->getUrl());
        $this->assertSame('2019-01-14T13:57:44+00:00', $response->getExpireAt()->toIso8601String());
        $this->assertSame('2019-01-14T13:42:44+00:00', $response->getCreatedAt()->toIso8601String());
        $this->assertSame(InvoiceStatus::NEW, $response->getStatus()->getValue());
        $this->assertSame('0.00002', $response->getBaseAmount());
        $this->assertSame('BTC', $response->getBaseCurrency());
        $this->assertSame('0.00002', $response->getAmount());
        $this->assertSame('0', $response->getPaidConfirmed());
        $this->assertSame('0', $response->getPaidUnconfirmed());
        $this->assertSame('0', $response->getPaid());
        $this->assertSame('0.00002', $response->getMissing());
        $this->assertSame('1', $response->getCurrencyRate());
    }
}
