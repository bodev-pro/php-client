<?php declare(strict_types=1);

namespace Tests\Unit\Payouts;

use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Handlers\PayoutStatusHandler;
use Tests\TestCase;

class StatusHandlerTest extends TestCase
{
    protected function tearDown()
    {
        unset($_SERVER['HTTP_HMAC']);
        parent::tearDown();
    }

    public function getHandler(): PayoutStatusHandler
    {
        return new PayoutStatusHandler(static::WEBHOOK_KEY);
    }

    /**
     * @group handler
     * @group payout
     */
    public function testSuccessfulPayoutStatusHandler()
    {
        $_SERVER['HTTP_HMAC'] = '6dce6755e62050451c808c3443d65e36a8c1c585f50fca28315264228b264422';
        $handler = $this->getHandler();
        $mockData = json_decode($this->getMockData('payouts/status-successful.json'), true);
        /** @var \Paycoiner\Client\Models\Webhooks\PayoutStatus $payoutStatus */
        $payoutStatus = $handler->handle($mockData);

        $this->assertEquals($mockData['payoutId'], $payoutStatus->getPayoutId());
        $this->assertEquals($mockData['customerId'], $payoutStatus->getCustomerId());
        $this->assertEquals($mockData['orderId'], $payoutStatus->getOrderId());
        $this->assertEquals($mockData['fee'], $payoutStatus->getFee());
    }

    /**
     * @group handler
     * @group payout
     */
    public function testPayoutStatusHandlerWithIncorrectHash()
    {
        $this->expectException(InvalidHash::class);

        $clientMock = $this->getHandler();
        $clientMock->handle(json_decode($this->getMockData('payouts/status-successful.json'), true));
    }

    /**
     * @group handler
     * @group payout
     */
    public function testPayoutStatusHandlerWithCorrectHashAndIncorrectData()
    {
        $this->expectException(ValidationException::class);

        $_SERVER['HTTP_HMAC'] = '2c28787ac1e58cc5ad9f353848cca0664da04f4b9f4fb0e47896275e356e9f09';
        $clientMock = $this->getHandler();
        $clientMock->handle(json_decode($this->getMockData('webhook-incorrect-data.json'), true));
    }
}
