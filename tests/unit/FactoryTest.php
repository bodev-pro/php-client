<?php declare(strict_types=1);

namespace Tests\Unit;

use Paycoiner\Client\Enums\HttpStatus;
use Paycoiner\Client\Exceptions\EnvironmentException;
use Paycoiner\Client\Factory;
use Paycoiner\Client\Models\Environment;
use Paycoiner\Client\Models\Requests\GetNextAddressRequest;
use Paycoiner\Client\Models\Requests\PayoutRequest;
use Paycoiner\Client\Models\Responses\DepositAddress;
use Paycoiner\Client\Models\Responses\Ticket;
use Paycoiner\Client\Models\Webhooks\DepositReceived;
use Tests\TestCase;

class FactoryTest extends TestCase
{
    /** @var Factory */
    private $paycoinerFactory;

    protected function setUp()
    {
        parent::setUp();
        $this->paycoinerFactory = new Factory();
    }

    protected function tearDown()
    {
        unset($_SERVER['HTTP_HMAC']);
        parent::tearDown();
    }

    /**
     * @group factory
     */
    public function testPayoutClient()
    {
        $mockedClient = $this->getMockedGuzzleClient(HttpStatus::OK(), 'payouts/successfully-created.json');
        $payoutRequest = PayoutRequest::fromArray(
            json_decode($this->getMockData('payouts/successfully-broadcasted.json'), true)
        );
        $this->paycoinerFactory->getClientFactory()->getPayoutClient()->setClient($mockedClient);
        /** @var \Paycoiner\Client\Models\Responses\CreatedPayout $payoutResponse */
        $payoutResponse = $this->paycoinerFactory->callCreatePayout($payoutRequest);

        $this->assertEquals($payoutResponse->getCustomerId(), $payoutRequest->customerId);
        $this->assertEquals($payoutResponse->getOrderId(), $payoutRequest->orderId);
        $this->assertNotNull($payoutResponse->getPayoutId());
    }

    /**
     * @group factory
     */
    public function testPayoutStatusHandler()
    {
        $_SERVER['HTTP_HMAC'] = '6dce6755e62050451c808c3443d65e36a8c1c585f50fca28315264228b264422';
        $mockData = json_decode($this->getMockData('payouts/status-successful.json'), true);
        /** @var \Paycoiner\Client\Models\Webhooks\PayoutStatus $payoutStatus */
        $payoutStatus = $this->paycoinerFactory->handlePayoutStatus($mockData);

        $this->assertEquals($mockData['payoutId'], $payoutStatus->getPayoutId());
        $this->assertEquals($mockData['customerId'], $payoutStatus->getCustomerId());
        $this->assertEquals($mockData['orderId'], $payoutStatus->getOrderId());
        $this->assertEquals($mockData['fee'], $payoutStatus->getFee());
    }

    /**
     * @group factory
     */
    public function testDepositHandler()
    {
        $_SERVER['HTTP_HMAC'] = 'faed13a8802f14687ae6bc4cf47a89c700857ed40ae5fe03555eb4baf529bf7b';
        $mockData = json_decode($this->getMockData('deposits/successful.json'), true);
        /** @var DepositReceived $depositReceived */
        $depositReceived = $this->paycoinerFactory->handleDepositReceived($mockData);

        $this->assertEquals($mockData['depositId'], $depositReceived->getDepositId());
        $this->assertEquals($mockData['customerId'], $depositReceived->getCustomerId());
        $this->assertEquals($mockData['txId'], $depositReceived->getTxId());
        $this->assertEquals($mockData['address'], $depositReceived->getAddress());
        $this->assertEquals($mockData['amount'], $depositReceived->getAmount());
        $this->assertEquals($mockData['currency'], $depositReceived->getCurrency());
        $this->assertEquals($mockData['confirmations'], $depositReceived->getConfirmations());
    }

    /**
     * @group factory
     */
    public function testDepositAddressClient()
    {
        $mockedClient = $this->getMockedGuzzleClient(HttpStatus::OK(), 'deposits/address.json');
        $this->paycoinerFactory->getClientFactory()->getDepositAddressClient()->setClient($mockedClient);
        $addressRequest = new GetNextAddressRequest(
            'e9d43ed2-f253-11e8-9179-fa163ee532b5',
            '1547473364088',
            'BTC'
        );

        $this->assertAddressResponse($this->paycoinerFactory->callGetNextDepositAddress($addressRequest));
    }

    /**
     * @group factory
     */
    public function testSuccessfullyGetAddressInfo()
    {
        $mockedClient = $this->getMockedGuzzleClient(HttpStatus::OK(), 'deposits/address.json');
        $this->paycoinerFactory->getClientFactory()->getDepositAddressClient()->setClient($mockedClient);

        $this->assertAddressResponse($this->paycoinerFactory->callGetDepositAddressInfo('d17e5e28-23b1-11e9-a439-507b9dfbcb8f'));
    }

    public function testInvalidEnvironment()
    {
        $factory = new Factory(new Environment()); // create empty environment
        $this->expectException(EnvironmentException::class);

        // call any function and receive exception
        $factory->callGetDepositAddressInfo('d17e5e28-23b1-11e9-a439-507b9dfbcb8f');
    }

    public function testExchangeRatesGetCurrencies()
    {
        $mockedClient = $this->getMockedGuzzleClient(HttpStatus::OK(), 'exchange-rates/currencies.json');
        $this->paycoinerFactory->getClientFactory()->getExchangeRatesClient()->setClient($mockedClient);
        $this->assertCount(23, $this->paycoinerFactory->getClientFactory()->getExchangeRatesClient()->getCurrencies());
    }

    public function testExchangeRatesGetHistoricalCurrencies()
    {
        $mockedClient = $this->getMockedGuzzleClient(HttpStatus::OK(), 'exchange-rates/historical-currencies.json');
        $this->paycoinerFactory->getClientFactory()->getExchangeRatesClient()->setClient($mockedClient);
        $this->assertCount(21, $this->paycoinerFactory->getClientFactory()->getExchangeRatesClient()->getCurrencies());
    }

    public function testExchangeRatesGetTicket()
    {
        $mockedClient = $this->getMockedGuzzleClient(HttpStatus::OK(), 'exchange-rates/ticket.json');
        $this->paycoinerFactory->getClientFactory()->getExchangeRatesClient()->setClient($mockedClient);
        $ticket = $this->paycoinerFactory->getClientFactory()->getExchangeRatesClient()->getTicket('usd', 'btc');
        $this->assertInstanceOf(Ticket::class, $ticket);
        $this->assertSame('2deb019c-ff91-11e8-98ba-507b9dfbcb8f', $ticket->getUuid());
        $this->assertSame('USD', $ticket->getBase());
        $this->assertSame('BTC', $ticket->getQuote());
        $this->assertSame('0.00030623', $ticket->getRate());
        $this->assertSame(1544785964, $ticket->getTimestamp());
    }

    public function testExchangeRatesGetHistoricalTicket()
    {
        $mockedClient = $this->getMockedGuzzleClient(HttpStatus::OK(), 'exchange-rates/historical-ticket.json');
        $this->paycoinerFactory->getClientFactory()->getExchangeRatesClient()->setClient($mockedClient);
        $ticket = $this->paycoinerFactory->getClientFactory()->getExchangeRatesClient()->getTicket('usd', 'btc', 1542170000);
        $this->assertInstanceOf(Ticket::class, $ticket);
        $this->assertSame('28fa0f0c-ff91-11e8-954b-507b9dfbcb8f', $ticket->getUuid());
        $this->assertSame('USD', $ticket->getBase());
        $this->assertSame('BTC', $ticket->getQuote());
        $this->assertSame('0.000176047349695174015130312936', $ticket->getRate());
        $this->assertSame(1542236399, $ticket->getTimestamp());
    }

    private function assertAddressResponse(DepositAddress $response)
    {
        $this->assertEquals('1547473364088', $response->getOrderId());
        $this->assertEquals('BTC', $response->getCurrency());
        $this->assertEquals('d17e5e28-23b1-11e9-a439-507b9dfbcb8f', $response->getId());
        $this->assertEquals('rlEVA1TFr3QeRFoNlYM7LufsRrA8T', $response->getAddress());
        $this->assertEquals(
            'o34CoumNxCmnYMVd8qu2LQfWLdY3IwXrnUzYa01yu1l+tnDrnpH1S/svFy9p/abkMksLpgyC9KvGdAIvw/gFQ3K1Z8/NRbKZWqJN2Tt4AuZfeDI0n3B/VCNkmLDIPhu0lPnlJEsb9KAvPNlCPcYuLdO0VKUag6cVz41lv7AQAaM=',
            $response->getSignature()
        );
    }
}
