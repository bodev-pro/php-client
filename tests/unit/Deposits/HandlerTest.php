<?php declare(strict_types=1);

namespace Tests\Unit\Deposits;

use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Handlers\DepositReceivedHandler;
use Paycoiner\Client\Models\Webhooks\DepositReceived;
use Tests\TestCase;

class HandlerTest extends TestCase
{
    protected function tearDown()
    {
        unset($_SERVER['HTTP_HMAC']);
        parent::tearDown();
    }

    public function getHandler(): DepositReceivedHandler
    {
        return new DepositReceivedHandler(static::WEBHOOK_KEY);
    }
    /**
     * @group handler
     * @group deposit
     */
    public function testSuccessfulDepositReceivedHandler()
    {
        $handler = $this->getHandler();
        $_SERVER['HTTP_HMAC'] = 'faed13a8802f14687ae6bc4cf47a89c700857ed40ae5fe03555eb4baf529bf7b';
        $mockData = json_decode($this->getMockData('deposits/successful.json'), true);
        /** @var DepositReceived $depositReceived */
        $depositReceived = $handler->handle($mockData);

        $this->assertEquals($mockData['depositId'], $depositReceived->getDepositId());
        $this->assertEquals($mockData['customerId'], $depositReceived->getCustomerId());
        $this->assertEquals($mockData['txId'], $depositReceived->getTxId());
        $this->assertEquals($mockData['address'], $depositReceived->getAddress());
        $this->assertEquals($mockData['amount'], $depositReceived->getAmount());
        $this->assertEquals($mockData['currency'], $depositReceived->getCurrency());
        $this->assertEquals($mockData['confirmations'], $depositReceived->getConfirmations());
    }
    /**
     * @group handler
     * @group deposit
     */
    public function testDepositReceivedHandlerWithIncorrectData()
    {
        $this->expectException(InvalidHash::class);

        $clientMock = $this->getHandler();
        $clientMock->handle([]);
    }

    /**
     * @group handler
     * @group deposit
     */
    public function testDepositReceivedHandlerWithIncorrectHash()
    {
        $this->expectException(InvalidHash::class);

        $_SERVER['HTTP_HMAC'] = 'dc64dec4c0baa938e326bd4efef2defc8185964b4f612361e7cc8d7712c3d0cc';
        $clientMock = $this->getHandler();
        $clientMock->handle(json_decode($this->getMockData('deposits/successful.json'), true));
    }

    /**
     * @group handler
     * @group deposit
     */
    public function testDepositReceivedHandlerWithCorrectHashAndIncorrectData()
    {
        $this->expectException(ValidationException::class);

        $_SERVER['HTTP_HMAC'] = '2c28787ac1e58cc5ad9f353848cca0664da04f4b9f4fb0e47896275e356e9f09';
        $clientMock = $this->getHandler();
        $clientMock->handle(json_decode($this->getMockData('webhook-incorrect-data.json'), true));
    }
}
