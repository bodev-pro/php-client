<?php declare(strict_types=1);

namespace Tests;

use Dotenv\Dotenv;
use Faker\Factory;
use Faker\Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Paycoiner\Client\Enums\HttpStatus;
use PHPUnit\Framework\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    const EMPTY_RESPONSE_FILE = 'empty-response.json';
    const ENDPOINT_KEY = '12345';
    const WEBHOOK_KEY = '67890';

    /** @var Generator */
    protected $faker;

    public function getMockedGuzzleClient(HttpStatus $status = null, string $mockPath = null): Client
    {
        if ($status === null) {
            $status = HttpStatus::OK();
        }

        $response = new Response(
            $status->getValue(),
            [],
            $this->getMockData($mockPath ?? static::EMPTY_RESPONSE_FILE)
        );
        if ($status->getValue() >= HttpStatus::BAD_REQUEST) {
            $response = new BadResponseException('Test', new Request('post', ''), $response);
        }

        $mockHandler = new MockHandler([$response]);
        $handlerStack = HandlerStack::create($mockHandler);

        return new Client(['handler' => $handlerStack]);
    }

    public function getMockData(string $path): string
    {
        return file_get_contents(__DIR__ . '/mocks/' . $path);
    }

    protected function setUp()
    {
        parent::setUp();
        (new Dotenv(__DIR__ . '/..', '.env.testing'))->load();
        $this->faker = Factory::create();
    }
}
