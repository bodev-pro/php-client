<?php declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use Exception;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\RequestOptions;
use Paycoiner\Client\Enums\HttpMethod;
use Paycoiner\Client\Enums\HttpStatus;
use Paycoiner\Client\Exceptions\Endpoints\NotFound;
use Paycoiner\Client\Exceptions\Endpoints\UnauthorizedRequest;
use Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest;
use Paycoiner\Client\Exceptions\PaycoinerClientException;
use Paycoiner\Client\Models\Responses\Response;
use Psr\Http\Message\ResponseInterface;

abstract class Client
{
    const DEFAULT_CLIENT_TIMEOUT = 10;
    const UNKNOWN_FLAG = 'UNKNOWN';

    /** @var string */
    protected $baseUri;
    /** @var string */
    protected $endpointKey;
    /** @var HttpClient */
    protected $client;

    public function __construct(string $baseUri, string $endpointKey = null, int $clientTimeOut = null)
    {
        $this->endpointKey = $endpointKey;
        $this->baseUri = $baseUri;
        if (!preg_match('/\/$/', $this->baseUri)) {
            $this->baseUri .= '/';
        }
        if (!$clientTimeOut) {
            $clientTimeOut = static::DEFAULT_CLIENT_TIMEOUT;
        }

        $this->client = new HttpClient([
            'timeout' => $clientTimeOut,
            'headers' => [
                'Content-type' => $this->getContentType(),
                'Accept' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
            ],
        ]);
    }

    public function getContentType(): string
    {
        return 'application/json';
    }

    public function setClient(HttpClient $client)
    {
        $this->client = $client;
    }

    public function getRequestBodyType(HttpMethod $httpMethod): string
    {
        return $httpMethod->isEquals(HttpMethod::GET()) ? RequestOptions::QUERY : RequestOptions::JSON;
    }

    /**
     * @throws GuzzleException
     * @throws PaycoinerClientException
     * @throws UnprocessableRequest
     * @throws UnauthorizedRequest
     * @throws NotFound
     */
    final protected function send(HttpMethod $method, string $url, array $data = [], array $headers = []): Response
    {
        if ($method->isNotEquals(HttpMethod::GET())) {
            $this->appendHashToRequest($data, $headers);
        }
        try {
            $response = $this->client->request(
                $method->getValue(),
                $this->baseUri . $url,
                [
                    $this->getRequestBodyType($method) => $data,
                    RequestOptions::HEADERS => $headers,
                ]
            );
        } catch (RequestException $e) {
            throw $this->getExceptionForResponse($e->getResponse(), $e);
        } catch (TransferException $e) {
            throw $this->getExceptionForResponse(null, $e);
        }

        $responseData = json_decode((string) $response->getBody(), true);
        if ($responseData !== null) {
            return new Response(new HttpStatus($response->getStatusCode()), $responseData);
        }

        throw $this->getExceptionForResponse($response);
    }

    /**
     * @param array $data
     * @param array $headers
     *
     * @return void
     */
    abstract protected function appendHashToRequest(&$data, &$headers);

    /**
     * @return UnprocessableRequest|UnauthorizedRequest|UnprocessableRequest|NotFound|PaycoinerClientException|null
     */
    protected function getExceptionForResponse(ResponseInterface $response = null, Exception $e = null)
    {
        if ($response === null) {
            return new UnprocessableRequest(static::UNKNOWN_FLAG, '', $e);
        }

        $responseBody = (string) $response->getBody();
        $result = json_decode($responseBody, true);

        // something went wrong with cryptoApi
        $flag = $result['flag'] ?? static::UNKNOWN_FLAG;
        $data = $result['data'] ?? $responseBody;

        if ($response->getStatusCode() === HttpStatus::NOT_FOUND) {
            return new NotFound($flag, json_encode($data), $e);
        }
        if ($response->getStatusCode() === HttpStatus::UNAUTHORIZED) {
            return new UnauthorizedRequest($flag, json_encode($data), $e);
        }
        if ($response->getStatusCode() === HttpStatus::UNPROCESSABLE_ENTITY) {
            return new UnprocessableRequest($flag, json_encode($data), $e);
        }

        if ($e === null) {
            return null;
        }

        return new PaycoinerClientException($flag, json_encode($data), $e);
    }
}
