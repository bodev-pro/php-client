<?php declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use Paycoiner\Client\Exceptions\EnvironmentException;
use Paycoiner\Client\Exceptions\Jwt\InvalidKey;
use Paycoiner\Client\Models\Environment;

class ClientFactory
{
    /** @var Environment */
    private $environment;

    /** @var PayoutClient|null */
    private $payoutClient;

    /** @var PaymentClient */
    private $paymentClient;

    /** @var DepositAddressClient|null */
    private $depositAddressClient;

    /** @var AddressValidatorClient|null */
    private $addressValidatorClient;

    /** @var ExchangeRatesClient|null */
    private $exchangeRatesClient;

    public function __construct(Environment $environment = null)
    {
        if ($environment === null) {
            $environment = Environment::loadFromEnv();
        }
        $this->environment = $environment;
    }

    /** @throws EnvironmentException */
    private function clientInitializer(string $clientClass, string $url = null, Client &$client = null, string $key = null, bool $keyRequired = true): Client
    {
        if ($client !== null) {
            return $client;
        }
        if (! $url || ($keyRequired && !$key)) {
            throw new EnvironmentException('The Environment should have url' . ($key ? ' and endpointKey/privateKey' : '') . ' for ' . $clientClass . ' initialization.');
        }
        $client = new $clientClass($url, $key);

        return $client;
    }

    /** @throws EnvironmentException */
    public function getPaymentClient(): PaymentClient
    {
        $this->clientInitializer(
            PaymentClient::class,
            $this->environment->getPaymentsUrl(),
            $this->paymentClient,
            $this->environment->getEndpointKey()
        );

        return $this->paymentClient;
    }

    /**
     * @throws InvalidKey
     * @throws EnvironmentException
     */
    public function getPayoutClient(): PayoutClient
    {
        $this->clientInitializer(
            PayoutClient::class,
            $this->environment->getPayoutsUrl(),
            $this->payoutClient,
            $this->environment->getPrivateKey()
        );

        return $this->payoutClient;
    }

    /** @throws EnvironmentException */
    public function getAddressValidatorClient(): AddressValidatorClient
    {
        $this->clientInitializer(
            AddressValidatorClient::class,
            $this->environment->getAddressValidatorUrl(),
            $this->addressValidatorClient,
            null,
            false
        );

        return $this->addressValidatorClient;
    }

    /** @throws EnvironmentException */
    public function getDepositAddressClient(): DepositAddressClient
    {
        $this->clientInitializer(
            DepositAddressClient::class,
            $this->environment->getDepositAddressUrl(),
            $this->depositAddressClient,
            $this->environment->getWebhookKey()
        );

        return $this->depositAddressClient;
    }

    /** @throws EnvironmentException */
    public function getExchangeRatesClient(): ExchangeRatesClient
    {
        $this->clientInitializer(
            ExchangeRatesClient::class,
            $this->environment->getExchangeRatesUrl(),
            $this->exchangeRatesClient,
            null,
            false
        );

        return $this->exchangeRatesClient;
    }
}
