<?php declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use Paycoiner\Client\Enums\HttpMethod;
use Paycoiner\Client\Models\Responses\Ticket;

class ExchangeRatesClient extends Client
{
    const API_VERSION_PREFIX = 'api';

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     * @throws \Paycoiner\Client\Exceptions\ValidationException
     */
    public function getTicket(string $base, string $quote, int $timestamp = null): Ticket
    {
        return Ticket::fromArray(
            $this->send(
                HttpMethod::GET(),
                self::API_VERSION_PREFIX . '/tickets/' . $this->getCurrencyPair($base, $quote) . ($timestamp === null ? '' : '/' . $timestamp)
            )->getResponse()
        );
    }

    public function getCurrencyPair(string $base, string $quote): string
    {
        return strtoupper(urlencode($base) . '/' . urlencode($quote));
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     */
    public function isCurrencyPairSupported(string $base, string $quote): bool
    {
        return in_array($this->getCurrencyPair($base, $quote), $this->getCurrencies(), true);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     */
    public function getCurrencies(): array
    {
        return $this->send(HttpMethod::GET(), self::API_VERSION_PREFIX . '/currencies')->getResponse();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     */
    public function isHistoricalCurrencyPairSupported(string $base, string $quote): bool
    {
        return in_array($this->getCurrencyPair($base, $quote), $this->getHistoricalCurrencies(), true);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     */
    public function getHistoricalCurrencies(): array
    {
        return $this->send(HttpMethod::GET(), self::API_VERSION_PREFIX . '/currencies/historical')->getResponse();
    }

    protected function appendHashToRequest(&$data, &$headers)
    {
        // Nothing to append
    }
}
