<?php declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use GuzzleHttp\Exception\GuzzleException;
use Paycoiner\Client\Enums\HttpMethod;
use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Models\Requests\PayoutRequest;
use Paycoiner\Client\Models\Responses\CreatedPayout;

class PayoutClient extends JwtClient
{
    const API_VERSION_PREFIX = 'api/v2';

    /**
     * @throws GuzzleException
     * @throws ValidationException
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnauthorizedRequest
     * @throws \Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     */
    public function create(PayoutRequest $payoutRequest): CreatedPayout
    {
        $result = $this->send(HttpMethod::POST(), self::API_VERSION_PREFIX . '/payouts', $payoutRequest->toArray());

        return CreatedPayout::fromArray($result->getResponse());
    }
}
