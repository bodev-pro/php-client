<?php declare(strict_types=1);

namespace Paycoiner\Client\Services;

use Firebase\JWT\JWT;
use Paycoiner\Client\Enums\JwtAlgorithm;
use Paycoiner\Client\Exceptions\Jwt\InvalidKey;

class JwtService
{
    public function isValidPublicKey(string $publicKey): bool
    {
        return (bool) preg_match('/^-----BEGIN (RSA )?PUBLIC KEY-----(.*)-----END (RSA )?PUBLIC KEY-----$/s', $publicKey);
    }

    public function isValidPrivateKey(string $privateKey): bool
    {
        return (bool) preg_match('/^-----BEGIN (RSA )?PRIVATE KEY-----(.*)-----END (RSA )?PRIVATE KEY-----$/s', $privateKey);
    }

    /**
     * @throws InvalidKey
     */
    public function checkKeyFileExists(string $path)
    {
        if (file_exists($path)) {
            return;
        }

        throw new InvalidKey('File not exists.');
    }

    /**
     * @throws InvalidKey
     */
    public function getPublicKey(string $publicKeyOrPath): string
    {
        if ($this->isValidPublicKey($publicKeyOrPath)) {
            return $publicKeyOrPath;
        }

        $this->checkKeyFileExists($publicKeyOrPath);

        $publicKeyOrPath = file_get_contents($publicKeyOrPath);
        if ($this->isValidPublicKey($publicKeyOrPath)) {
            return $publicKeyOrPath;
        }

        throw new InvalidKey('Invalid public key.');
    }

    /**
     * @throws InvalidKey
     */
    public function getPrivateKey(string $privateKeyOrPath): string
    {
        if ($this->isValidPrivateKey($privateKeyOrPath)) {
            return $privateKeyOrPath;
        }

        $this->checkKeyFileExists($privateKeyOrPath);

        $privateKeyOrPath = file_get_contents($privateKeyOrPath);
        if ($this->isValidPrivateKey($privateKeyOrPath)) {
            return $privateKeyOrPath;
        }

        throw new InvalidKey('Invalid private key.');
    }

    public function encode(array $payload, string $privateKey, JwtAlgorithm $algorithm = null): string
    {
        return JWT::encode($payload, $privateKey, $algorithm ? $algorithm->getValue() : JwtAlgorithm::RS256);
    }

    public function decode(string $content, string $publicKey, array $algorithms = null): array
    {
        $algorithms === null ? JwtAlgorithm::getKeys() : array_map(function ($algorithm) {
            return (string) $algorithm;
        }, $algorithms);

        return (array) JWT::decode(
            $content,
            $publicKey,
            $algorithms
        );
    }
}
