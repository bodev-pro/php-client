<?php declare(strict_types=1);

namespace Paycoiner\Client\Handlers;

use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Models\Responses\Invoice;

class PaymentHandler extends HmacHandler
{
    /**
     * @throws InvalidHash
     * @throws ValidationException
     */
    public function handle(array $data): Invoice
    {
        $this->check($data);

        return Invoice::fromArray($data);
    }
}
