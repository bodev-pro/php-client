<?php declare(strict_types=1);

namespace Paycoiner\Client\Handlers;

use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Models\Webhooks\PayoutStatus;

class PayoutStatusHandler extends HmacHandler
{
    /**
     * @throws InvalidHash
     * @throws ValidationException
     */
    public function handle(array $data): PayoutStatus
    {
        $this->check($data);

        return PayoutStatus::fromArray($data);
    }
}
