<?php declare(strict_types=1);

namespace Paycoiner\Client\Validators;

use Paycoiner\Client\Exceptions\Jwt\InvalidKey;
use Paycoiner\Client\Models\Model;
use Paycoiner\Client\Services\JwtService;

class AddressSignatureValidator extends Validator
{

    /**
     * @throws InvalidKey
     */
    public function __construct(string $paycoinerPublicKeyOrPath)
    {
        $jwtService = new JwtService();
        parent::__construct($jwtService->getPublicKey($paycoinerPublicKeyOrPath));
    }


    public function isValid($data): bool
    {
        if ($data instanceof Model) {
            $data = $data->toArray();
        }
        if (false === is_array($data)) {
            return false;
        }
        if ($this->hasNotRequiredData($data)) {
            return false;
        }

        $signature = base64_decode($data['signature']);
        if (false === $signature) {
            return false;
        }

        return openssl_verify(
                $data['address'] . $data['currency'] . ($data['paymentTag'] ?? ''),
                $signature,
                $this->key,
                OPENSSL_ALGO_SHA256
            ) > 0;
    }

    protected function hasNotRequiredData(array $data): bool
    {
        return false === isset($data['address'], $data['currency'], $data['signature']);
    }
}
