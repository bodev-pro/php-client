<?php declare(strict_types=1);

namespace Paycoiner\Client\Enums;

/**
 * @method static static CONTINUE()
 * @method static static SWITCHING_PROTOCOLS()
 * @method static static PROCESSING()
 * @method static static OK()
 * @method static static CREATED()
 * @method static static ACCEPTED()
 * @method static static NON_AUTHORITATIVE_INFORMATION()
 * @method static static NO_CONTENT()
 * @method static static RESET_CONTENT()
 * @method static static PARTIAL_CONTENT()
 * @method static static MULTI_STATUS()
 * @method static static ALREADY_REPORTED()
 * @method static static IM_USED()
 * @method static static MULTIPLE_CHOICES()
 * @method static static MOVED_PERMANENTLY()
 * @method static static FOUND()
 * @method static static SEE_OTHER()
 * @method static static NOT_MODIFIED()
 * @method static static USE_PROXY()
 * @method static static RESERVED()
 * @method static static TEMPORARY_REDIRECT()
 * @method static static PERMANENTLY_REDIRECT()
 * @method static static BAD_REQUEST()
 * @method static static UNAUTHORIZED()
 * @method static static PAYMENT_REQUIRED()
 * @method static static FORBIDDEN()
 * @method static static NOT_FOUND()
 * @method static static METHOD_NOT_ALLOWED()
 * @method static static NOT_ACCEPTABLE()
 * @method static static PROXY_AUTHENTICATION_REQUIRED()
 * @method static static REQUEST_TIMEOUT()
 * @method static static CONFLICT()
 * @method static static GONE()
 * @method static static LENGTH_REQUIRED()
 * @method static static PRECONDITION_FAILED()
 * @method static static REQUEST_ENTITY_TOO_LARGE()
 * @method static static REQUEST_URI_TOO_LONG()
 * @method static static UNSUPPORTED_MEDIA_TYPE()
 * @method static static REQUESTED_RANGE_NOT_SATISFIABLE()
 * @method static static EXPECTATION_FAILED()
 * @method static static I_AM_A_TEAPOT()
 * @method static static MISDIRECTED_REQUEST()
 * @method static static UNPROCESSABLE_ENTITY()
 * @method static static LOCKED()
 * @method static static FAILED_DEPENDENCY()
 * @method static static RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL()
 * @method static static UPGRADE_REQUIRED()
 * @method static static PRECONDITION_REQUIRED()
 * @method static static TOO_MANY_REQUESTS()
 * @method static static REQUEST_HEADER_FIELDS_TOO_LARGE()
 * @method static static UNAVAILABLE_FOR_LEGAL_REASONS()
 * @method static static INTERNAL_SERVER_ERROR()
 * @method static static NOT_IMPLEMENTED()
 * @method static static BAD_GATEWAY()
 * @method static static SERVICE_UNAVAILABLE()
 * @method static static GATEWAY_TIMEOUT()
 * @method static static VERSION_NOT_SUPPORTED()
 * @method static static VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL()
 * @method static static INSUFFICIENT_STORAGE()
 * @method static static LOOP_DETECTED()
 * @method static static NOT_EXTENDED()
 * @method static static NETWORK_AUTHENTICATION_REQUIRED()
 */
final class HttpStatus extends Enum
{
    const CONTINUE = 100;
    const SWITCHING_PROTOCOLS = 101;
    const PROCESSING = 102;            // RFC2518
    const OK = 200;
    const CREATED = 201;
    const ACCEPTED = 202;
    const NON_AUTHORITATIVE_INFORMATION = 203;
    const NO_CONTENT = 204;
    const RESET_CONTENT = 205;
    const PARTIAL_CONTENT = 206;
    const MULTI_STATUS = 207;          // RFC4918
    const ALREADY_REPORTED = 208;      // RFC5842
    const IM_USED = 226;               // RFC3229
    const MULTIPLE_CHOICES = 300;
    const MOVED_PERMANENTLY = 301;
    const FOUND = 302;
    const SEE_OTHER = 303;
    const NOT_MODIFIED = 304;
    const USE_PROXY = 305;
    const RESERVED = 306;
    const TEMPORARY_REDIRECT = 307;
    const PERMANENTLY_REDIRECT = 308;  // RFC7238
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const PAYMENT_REQUIRED = 402;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const METHOD_NOT_ALLOWED = 405;
    const NOT_ACCEPTABLE = 406;
    const PROXY_AUTHENTICATION_REQUIRED = 407;
    const REQUEST_TIMEOUT = 408;
    const CONFLICT = 409;
    const GONE = 410;
    const LENGTH_REQUIRED = 411;
    const PRECONDITION_FAILED = 412;
    const REQUEST_ENTITY_TOO_LARGE = 413;
    const REQUEST_URI_TOO_LONG = 414;
    const UNSUPPORTED_MEDIA_TYPE = 415;
    const REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    const EXPECTATION_FAILED = 417;
    const I_AM_A_TEAPOT = 418;                                               // RFC2324
    const MISDIRECTED_REQUEST = 421;                                         // RFC7540
    const UNPROCESSABLE_ENTITY = 422;                                        // RFC4918
    const LOCKED = 423;                                                      // RFC4918
    const FAILED_DEPENDENCY = 424;                                           // RFC4918
    const RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;   // RFC2817
    const UPGRADE_REQUIRED = 426;                                            // RFC2817
    const PRECONDITION_REQUIRED = 428;                                       // RFC6585
    const TOO_MANY_REQUESTS = 429;                                           // RFC6585
    const REQUEST_HEADER_FIELDS_TOO_LARGE = 431;                             // RFC6585
    const UNAVAILABLE_FOR_LEGAL_REASONS = 451;
    const INTERNAL_SERVER_ERROR = 500;
    const NOT_IMPLEMENTED = 501;
    const BAD_GATEWAY = 502;
    const SERVICE_UNAVAILABLE = 503;
    const GATEWAY_TIMEOUT = 504;
    const VERSION_NOT_SUPPORTED = 505;
    const VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL = 506;                        // RFC2295
    const INSUFFICIENT_STORAGE = 507;                                        // RFC4918
    const LOOP_DETECTED = 508;                                               // RFC5842
    const NOT_EXTENDED = 510;                                                // RFC2774
    const NETWORK_AUTHENTICATION_REQUIRED = 511;                             // RFC6585
}
