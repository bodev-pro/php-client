<?php declare(strict_types=1);

namespace Paycoiner\Client\Enums;

/**
 * @method static static GET()
 * @method static static POST()
 * @method static static PUT()
 * @method static static DELETE()
 */
final class HttpMethod extends Enum
{
   const GET = 'get';
   const POST = 'post';
   const PUT = 'put';
   const DELETE = 'delete';
}
