<?php declare(strict_types=1);

namespace Paycoiner\Client\Enums;

use ReflectionClass;
use UnexpectedValueException;
use BadMethodCallException;
use JsonSerializable;

/**
 * Class Enum
 * @package App\Enums
 *
 * Create an enum by implementing this class and adding class constants.
 *
 * @author Matthieu Napoli <matthieu@mnapoli.fr>
 * @author Daniel Costa <danielcosta@gmail.com>
 * @author Mirosław Filip <mirfilip@gmail.com>
 */
abstract class Enum implements JsonSerializable
{
    /**
     * Enum value
     *
     * @var mixed
     */
    protected $value;

    /**
     * Store existing constants in a static cache per object.
     *
     * @var array
     */
    protected static $cache = array();

    /**
     * Creates a new value of some type
     *
     * @param mixed $value
     *
     * @throws UnexpectedValueException if incompatible type is given.
     */
    public function __construct($value)
    {
        if (false === static::isValid($value)) {
            throw new UnexpectedValueException("Value '$value' is not part of the enum " . static::class);
        }
        $this->value = $value;
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return $this->getValue();
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Returns the enum key (i.e. the constant name).
     *
     * @return mixed
     */
    public function getKey()
    {
        return static::search($this->value);
    }

    /**
     * Returns all possible values as an array
     *
     * @return array Constant name in key, constant value in value
     */
    public static function toArray(): array
    {
        $class = get_called_class();
        if (!array_key_exists($class, static::$cache)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $reflection = new ReflectionClass($class);
            static::$cache[$class] = $reflection->getConstants();
        }

        return static::$cache[$class];
    }

    public static function getKeys(): array
    {
        return array_keys(self::toArray());
    }

    /**
     * @param mixed $value
     * @return null|string
     */
    public static function search($value)
    {
        return array_search($value, static::toArray(), true);
    }

    /**
     * @param mixed $value
     * @return null|string
     */
    public static function getName($value)
    {
        return static::search($value);
    }

    /**
     * Check if is valid enum value
     *
     * @param mixed $value
     * @return bool
     */
    public static function isValid($value): bool
    {
        return in_array($value, static::toArray(), true);
    }

    /**
     * Check that values are equal
     *
     * @param mixed $value
     * @param bool $strict
     * @return bool
     */
    public function isEquals($value, bool $strict = true): bool
    {
        if ($value instanceof self) {
            return $this->isEquals($value->getValue(), $strict);
        }

        return $strict ? $value === $this->getValue() : $value == $this->getValue();
    }

    public function isNotEquals($value, bool $strict = true): bool
    {
        return false === $this->isEquals($value, $strict);
    }

    /**
     * Check if is valid enum key
     *
     * @param mixed $key
     * @return bool
     */
    public static function isValidKey($key): bool
    {
        $array = static::toArray();

        return isset($array[$key]);
    }

    /**
     * Returns a value when called statically like so: MyEnum::SOME_VALUE() given SOME_VALUE is a class constant
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return static
     * @throws \BadMethodCallException
     */
    public static function __callStatic($name, $arguments)
    {
        $array = static::toArray();
        if (isset($array[$name])) {
            return new static($array[$name]);
        }
        throw new BadMethodCallException("No static method or enum constant '$name' in class " . get_called_class());
    }

    public static function instanceFromValue($value): self
    {
        return new static($value);
    }
}
