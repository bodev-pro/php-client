<?php declare(strict_types=1);

namespace Paycoiner\Client;

use Paycoiner\Client\Clients\ClientFactory;
use Paycoiner\Client\Exceptions\EnvironmentException;
use Paycoiner\Client\Exceptions\PaycoinerClientException;
use Paycoiner\Client\Handlers\HandlerFactory;
use Paycoiner\Client\Models\Environment;
use Paycoiner\Client\Models\Model;
use Paycoiner\Client\Models\Requests\AddressValidateRequest;
use Paycoiner\Client\Models\Requests\CreateInvoiceRequest;
use Paycoiner\Client\Models\Requests\GetNextAddressRequest;
use Paycoiner\Client\Models\Requests\PayoutRequest;
use Paycoiner\Client\Models\Responses\CreatedPayout;
use Paycoiner\Client\Models\Responses\DepositAddress;
use Paycoiner\Client\Models\Responses\Invoice;
use Paycoiner\Client\Models\Webhooks\DepositReceived;
use Paycoiner\Client\Models\Webhooks\PayoutStatus;
use Paycoiner\Client\Validators\ValidatorFactory;

class Factory
{
    private $clientFactory;
    private $handlerFactory;
    private $validatorFactory;

    public function __construct(Environment $environment = null)
    {
        if ($environment === null) {
            $environment = Environment::loadFromEnv();
        }
        $this->clientFactory = new ClientFactory($environment);
        $this->handlerFactory = new HandlerFactory($environment);
        $this->validatorFactory = new ValidatorFactory($environment);
    }

    /**
     * @throws Exceptions\Endpoints\UnauthorizedRequest
     * @throws Exceptions\Endpoints\UnprocessableRequest
     * @throws Exceptions\Jwt\InvalidKey
     * @throws Exceptions\ValidationException
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function callCreatePayout(PayoutRequest $request): CreatedPayout
    {
        return $this->getClientFactory()->getPayoutClient()->create($request);
    }

    /**
     * @throws Exceptions\Endpoints\UnauthorizedRequest
     * @throws Exceptions\Endpoints\UnprocessableRequest
     * @throws Exceptions\ValidationException
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function callCreatePaymentInvoice(CreateInvoiceRequest $request): Invoice
    {
        return $this->getClientFactory()->getPaymentClient()->createInvoice($request);
    }

    /**
     * @throws Exceptions\Endpoints\NotFound
     * @throws Exceptions\Endpoints\UnprocessableRequest
     * @throws Exceptions\ValidationException
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function callGetPaymentInvoice(string $invoiceId): Invoice
    {
        return $this->getClientFactory()->getPaymentClient()->getInvoice($invoiceId);
    }

    /**
     * @throws Exceptions\Endpoints\UnauthorizedRequest
     * @throws Exceptions\Endpoints\UnprocessableRequest
     * @throws Exceptions\ValidationException
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function callGetNextDepositAddress(GetNextAddressRequest $request): DepositAddress
    {
        return $this->getClientFactory()->getDepositAddressClient()->getNext($request);
    }

    /**
     * @throws Exceptions\Endpoints\NotFound
     * @throws Exceptions\Endpoints\UnprocessableRequest
     * @throws Exceptions\ValidationException
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function callGetDepositAddressInfo(string $depositAddressId): DepositAddress
    {
        return $this->getClientFactory()->getDepositAddressClient()->getInfo($depositAddressId);
    }

    /**
     * @throws Exceptions\Endpoints\UnprocessableRequest
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function callAddressIsValid(AddressValidateRequest $request): bool
    {
        return (bool) $this->getClientFactory()->getAddressValidatorClient()->validate($request);
    }

    /**
     * @throws Exceptions\ValidationException
     * @throws Exceptions\Webhooks\InvalidHash
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     */
    public function handleDepositReceived(array $data): DepositReceived
    {
        return $this->getHandlerFactory()->getDepositReceivedHandler()->handle($data);
    }

    /**
     * @throws Exceptions\ValidationException
     * @throws Exceptions\Webhooks\InvalidHash
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     */
    public function handlePayoutStatus(array $data): PayoutStatus
    {
        return $this->getHandlerFactory()->getPayoutStatusHandler()->handle($data);
    }

    /**
     * @throws Exceptions\ValidationException
     * @throws Exceptions\Webhooks\InvalidHash
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     */
    public function handlePaymentStatus(array $data): Invoice
    {
        return $this->getHandlerFactory()->getPaymentHandler()->handle($data);
    }

    /**
     * @param array|Model $data
     * @return bool
     * @throws Exceptions\Jwt\InvalidKey
     * @throws EnvironmentException
     * @throws PaycoinerClientException
     */
    public function validateAddressSignature($data): bool
    {
        return $this->getValidatorFactory()->getAddressSignatureValidator()->isValid($data);
    }

    public function getClientFactory(): ClientFactory
    {
        return $this->clientFactory;
    }

    public function getHandlerFactory(): HandlerFactory
    {
        return $this->handlerFactory;
    }

    public function getValidatorFactory(): ValidatorFactory
    {
        return $this->validatorFactory;
    }
}
