<?php declare(strict_types=1);

namespace Paycoiner\Client\Models;

use Paycoiner\Client\Exceptions\Jwt\InvalidKey;
use Paycoiner\Client\Services\JwtService;

class Environment extends Model
{
    /** @var string|null */
    protected $webhookKey;
    /** @var string|null */
    protected $endpointKey;
    /** @var string|null */
    protected $privateKey;
    /** @var string|null */
    protected $servicePublicKey;
    /** @var string|null */
    protected $payoutsUrl;
    /** @var string|null */
    protected $paymentsUrl;
    /** @var string|null */
    protected $depositAddressUrl;
    /** @var string|null */
    protected $addressValidatorUrl;
    /** @var string|null */
    protected $exchangeRatesUrl;

    public static function loadFromEnv(string $prefix = 'PAYCOINER_')
    {
        $parameters = (new self())->toArray();
        $dataToSet = [];
        foreach ($parameters as $parameter => $value) {
            $snakeCase = preg_replace('/(?<!^)([A-Z])/', '_\\1', $parameter);
            $value = getenv(strtoupper($prefix . $snakeCase));
            if ((bool) $value === false) {
                continue;
            }
            $dataToSet[$parameter] = $value;
        }
        if (empty($dataToSet)) {
            return new self();
        }

        return self::fromArray($dataToSet);
    }

    protected function getNotRequiredProperties(): array
    {
        return [
            'webhookKey',
            'endpointKey',
            'privateKey',
            'servicePublicKey',
            'payoutsUrl',
            'paymentsUrl',
            'depositAddressUrl',
            'addressValidatorUrl',
        ];
    }

    /** @return string|null */
    public function getWebhookKey()
    {
        return $this->webhookKey;
    }

    public function setWebhookKey(string $webhookKey = null)
    {
        $this->webhookKey = $webhookKey;
    }

    /** @return string|null */
    public function getEndpointKey()
    {
        return $this->endpointKey;
    }

    public function setEndpointKey(string $endpointKey = null)
    {
        $this->endpointKey = $endpointKey;
    }

    /** @return string|null */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    public function setPrivateKey(string $privateKeyOrPath = null)
    {
        if ($privateKeyOrPath === null) {
            $this->privateKey = $privateKeyOrPath;
            return;
        }
        try {
            $this->privateKey = (new JwtService())->getPrivateKey($privateKeyOrPath);
        } catch (InvalidKey $invalidKey) {
            $this->privateKey = null;
        }
    }

    /** @return string|null */
    public function getServicePublicKey()
    {
        return $this->servicePublicKey;
    }

    public function setServicePublicKey(string $paycoinerPublicKeyOrPath = null)
    {
        if ($paycoinerPublicKeyOrPath === null) {
            $this->servicePublicKey = $paycoinerPublicKeyOrPath;
            return;
        }
        try {
            $this->servicePublicKey = (new JwtService())->getPublicKey($paycoinerPublicKeyOrPath);
        } catch (InvalidKey $invalidKey) {
            $this->servicePublicKey = null;
        }
    }

    /** @return string|null */
    public function getPayoutsUrl()
    {
        return $this->payoutsUrl;
    }

    public function setPayoutsUrl(string $payoutsUrl = null)
    {
        $this->payoutsUrl = $payoutsUrl;
    }

    /** @return string|null */
    public function getPaymentsUrl()
    {
        return $this->paymentsUrl;
    }

    public function setPaymentsUrl(string $paymentsUrl = null)
    {
        $this->paymentsUrl = $paymentsUrl;
    }

    /** @return string|null */
    public function getDepositAddressUrl()
    {
        return $this->depositAddressUrl;
    }

    public function setDepositAddressUrl(string $depositAddressUrl = null)
    {
        $this->depositAddressUrl = $depositAddressUrl;
    }

    /** @return string|null */
    public function getAddressValidatorUrl()
    {
        return $this->addressValidatorUrl;
    }

    public function setAddressValidatorUrl(string $addressValidatorUrl = null)
    {
        $this->addressValidatorUrl = $addressValidatorUrl;
    }

    public function getExchangeRatesUrl(): string
    {
        return $this->exchangeRatesUrl;
    }

    public function setExchangeRatesUrl(string $exchangeRatesUrl)
    {
        $this->exchangeRatesUrl = $exchangeRatesUrl;
    }
}
