<?php declare(strict_types=1);

namespace Paycoiner\Client\Models\Requests;

use Paycoiner\Client\Models\Model;

class AddressValidateRequest extends Model
{
    /**
     * The address to send the funds.
     * @var string
     */
    public $address;

    /**
     * Cryptocurrency shortcut (ex. BTC)
     * @var string
     */
    public $currency;

    /**
     * Transaction identifier. Only for specific currencies
     * @var string
     */
    public $paymentTag;

    public function __construct(string $address, string $currency, string $paymentTag = '')
    {
        $this->address = $address;
        $this->currency = $currency;
        $this->paymentTag = $paymentTag;
    }
}
