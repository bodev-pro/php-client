<?php declare(strict_types=1);

namespace Paycoiner\Client\Exceptions\Endpoints;

use Paycoiner\Client\Exceptions\PaycoinerClientException;

class UnprocessableRequest extends PaycoinerClientException
{
}
