<?php declare(strict_types=1);

namespace Paycoiner\Client\Exceptions\Endpoints;

use Paycoiner\Client\Exceptions\PaycoinerClientException;

class NotFound extends PaycoinerClientException
{
}
