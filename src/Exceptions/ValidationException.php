<?php declare(strict_types=1);

namespace Paycoiner\Client\Exceptions;

class ValidationException extends PaycoinerClientException
{
    public function __construct(string $message, \Throwable $previous = null)
    {
        parent::__construct('INVALID_RESPONSE', $message, $previous);
    }
}
