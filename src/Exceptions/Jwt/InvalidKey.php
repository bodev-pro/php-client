<?php declare(strict_types=1);

namespace Paycoiner\Client\Exceptions\Jwt;

use Exception;

class InvalidKey extends Exception
{
}
